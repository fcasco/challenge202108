"""
Refactor the next function using yield to return the array of objects found by the
`s3.list_objects_v2` function that matches the given prefix.
"""


import boto3


def get_s3_objects(bucket, prefix=''):
    s3 = boto3.client('s3')

    kwargs = {'Bucket': bucket}
    next_token = None

    if prefix:
        kwargs['Prefix'] = prefix

    while True:
        if next_token:
            kwargs['ContinuationToken'] = next_token

        resp = s3.list_objects_v2(**kwargs)
        contents = resp.get('Contents', [])

        for obj in contents:
            key = obj['Key']
            if key.startswith(prefix):
                yield(obj)

        next_token = kwargs.get('NextContinuationToken', None)

        if not next_token:
            break

    return


"""
Please, explain and document iterations, conditionals, and the
function as a whole
"""


def fn(main_plan, obj, str_id, pm, extensions=[]):
    """
        Receives a main plan and a list of objects and extensions
        First it extracs quantity information from the extensions
        Then it extracts the items from obj
        If the item is from an extension it gets the quantity from the extension
        If no item has the same id as the main_plan it adds the main_plan
        Then it adds the values from the extensions to the items
        Returns a list of items with an id, price and quantity
    """
    items = []
    sp = False
    cd = False

    # creates the dict to map price id with quantity from the extensions
    ext_p = {}

    for ext in extensions:
        # adds to ext_p the quantity for the price in ext with the price id
        ext_p[ext['price'].id] = ext['qty']

    for item in obj['items'].data:
        product = {
            'id': item.id
        }

        if item.price.id != main_plan.id and item.price.id not in ext_p:
            product['deleted'] = True
            cd = True
        elif item.price.id in ext_p:
            qty = ext_p[item.price.id]
            if qty < 1:
                product['deleted'] = True
            else:
                product['qty'] = qty
            del ext_p[item.price.id]
        elif item.price.id == main_plan.id:
            sp = True

        items.append(product)
    
    if not sp:
        items.append({
            'price': main_plan.id,
            'quantity': 1
        })
    
    for price, qty in ext_p.items():
        if qty < 1:
            continue
        items.append({
            'price': price,
            'quantity': qty
        })
    
    kwargs = {
        'items': items,
        'default_payment_method': pm.id,
        'api_key': API_KEY,
    }
    
    return items


"""
Having the class `Caller` and the function `fn`
Refactor the function `fn` to execute any method from `Caller` using the argument `fn_to_call`
reducing the `fn` function to only one line.
"""
class Caller:
    add = lambda a, b : a + b
    concat = lambda a, b : f'{a},{b}'
    divide = lambda a, b : a / b
    multiply = lambda a, b : a * b


def fn(fn_to_call, *args):
    return getattr(Caller, fn_to_call)(*args)


"""
A video transcoder was implemented with different presets to process different
videos in the application. The videos should be encoded with a given configuration
done by this function. Can you explain what this function is detecting from the params
and returning based in its conditionals?
"""

def fn(config, w, h):
    """This function detects the video aspect ratio
        From the aspect ratio determines if the video is in portrait, landscape or square format
        Based on that applies the settings for each format from the `config` object
    """
    v = None
    ar = w / h

    if ar < 1:
        v = [r for r in config['p'] if r['width'] <= w]
    elif ar > 4 / 3:
        v = [r for r in config['l'] if r['width'] <= w]
    else:
        v = [r for r in config['s'] if r['width'] <= w]

    return v


"""
Having the next helper, please implement a refactor to perform the API call
using one method instead of rewriting the code in the other methods.
"""
import requests


class Helper:
    DOMAIN = 'http://example.com'
    SEARCH_IMAGES_ENDPOINT = 'search/images'
    GET_IMAGE_ENDPOINT = 'image'
    DOWNLOAD_IMAGE_ENDPOINT = 'downloads/images'

    AUTHORIZATION_TOKEN = {
        'access_token': None,
        'token_type': None,
        'expires_in': 0,
        'refresh_token': None
    }

    def send_request(self, method, url_path, params):
        """Send a request to the images API
            :param method: HTTP method to send
            :param url_path: the path in the url where the request is sent
            :param params: url paramas sent in the request
        """
        token_type = self.AUTHORIZATION_TOKEN['token_type']
        access_token = self.AUTHORIZATION_TOKEN['access_token']

        headers = {
            'Authorization': f'{token_type} {access_token}',
        }

        URL = f'{self.DOMAIN}/{url_path}'

        if method not in ('get', 'post'):
            raise Exception(f'The method {method} should be either get or post')

        requests_method = getattr(requests, method)

        response = requests_method(URL, {'headers': headers, **params})

        return response

    def search_images(self, **kwargs):
        return self.send_request('get', self.SEARCH_IMAGES_ENDPOINT,
                                 {'params': kwargs})

    def get_image(self, image_id, **kwargs):
        return self.send_request('get', f'self.GET_IMAGE_ENDPOINT/{image_id}',
                                 {'params': kwargs})

    def download_image(self, image_id, **kwargs):
        return self.send_request('post', f'self.DOWNLOAD_IMAGE_ENDPOINT/{image_id}',
                                 {'data': kwargs})
